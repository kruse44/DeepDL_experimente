# -*- coding: utf-8 -*-
"""
Created on Wed Sep 18 09:18:53 2019

@author: L. Dammann
"""

import tensorflow as tf
import numpy as np

##################
# Regularization #
##################

def reg_matrix(diff_order, dim):
    '''
    dim is the number of columns of the designmatrix (number of knots + degree - 1)
    '''
    I = np.identity(dim, dtype = 'float32')
    D_t = np.diff(I, n = diff_order)
    K = np.matmul(D_t, D_t.T)
    return K

def reg_matrix_diag(reg_matrix):
    ''' 
    Returns diagonalized regularization matrix and orthogonal matrix of
    eigenvectors U. Note: reg_matrix = U * reg_matrix_diag * U.T 
    '''
    eigval, U = np.linalg.eigh(reg_matrix)
    reg_matrix_diag = np.diag(eigval)
    return (reg_matrix_diag, U)

def pen (reg_matrix, weight_matrix, reg_param):
    pen = tf.tensordot(tf.transpose(weight_matrix), 
                       tf.tensordot(reg_matrix, weight_matrix, axes = 1), axes = 1)
    return reg_param*pen


a = reg_matrix(diff_order=2, dim=5)
b, eigval = reg_matrix_diag(a)
