# -*- coding: utf-8 -*-
"""
Created on Fri Aug 30 11:39:41 2019

@author: L. Dammann & B. Saefken
"""
"""to Do: 2-dim gcv reinnehmen, centering for both variables
predictors: c_age, c_breastf; criterion: zscore
    """

## tf.VERSION is 1.13.1
## Python is version 3.6.9

import pandas as pd
import tensorflow as tf
import numpy as np

# import matplotlib.pyplot as plt
# import bsplines as bs
# import penalty as pen
# import gcv_derivative as gcv
import os as os

# own Code:
import deepdl_experimente.bsplines as bs
import deepdl_experimente.penalty as pen
import deepdl_experimente.gcv_derivative as gcv

n_knots = 20
degree_bsplines = 3
penalty_diff_order = 2
dim_reg_matrix = n_knots + degree_bsplines - 1
dim_reg_matrix_center = dim_reg_matrix - 1

stddev = 0.10  # for weight initialization of spline part
mean = 0.0  # for weight initialization of spline part
beta_0_init = 0.0  # 0.0 for zscore_norm, -171.2 for zscore
grid = np.array(
    [0.1, 1, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
)  # values for search of starting point of reg_params
# reg_param_c_age_init = 10.0         # 32.0 - Werte aus mgcv?
# reg_param_c_breastf_init = 10.0     # 21.0 - Werte aus mgcv?

num_epochs = 5000
num_steps_gcv = 1
learning_rate_gcv = 10.0
learning_rate_adam = 0.01

# dirname = os.path.dirname(__file__)     # does not work if only current line instead of whole file is executed
dirname = os.getcwd()  # if directory is set

# zambia = pd.read_table(os.path.join(dirname, 'zambia_height92.txt'))
# zambia = pd.read_csv(os.path.join(dirname, 'zambia_train.csv'))
zambia = pd.read_csv("deepdl_experimente/zambia_train.csv")

# zambia.columns            # gives variable names
# zambia.loc[0, 'zscore']   # choose rows and columns by label
# zambia.iloc[0,0]          # choose rows and columns by position


c_age_d = zambia.loc[:, "c_age"]
c_breastf_d = zambia.loc[:, "c_breastf"]

## Normalization before datasplit
labels = zambia.loc[:, "zscore_norm"]
labels_expanded = np.expand_dims(labels, 1)

## Normalization after datasplit
# labels = zambia.loc[:, 'zscore']
# labels = (labels - labels.mean()) / labels.std()
# labels_expanded = np.expand_dims(labels, 1)

base_c_age = bs.build_base(c_age_d, n_knots=n_knots, degree=degree_bsplines)
design_matrix_c_age = base_c_age[0]
# Constrain matrix C
C_c_age = design_matrix_c_age.sum(axis=0).reshape(-1, 1)
# QR of C -> leave out first column of q
q, r = np.linalg.qr(C_c_age, mode="complete")
# penalty matrix without centering -> dim p
K_c_age = pen.reg_matrix(penalty_diff_order, dim=dim_reg_matrix)
# penalty matrix with centering -> dim (p-1)
S_c_age = np.float32(np.linalg.multi_dot([np.transpose(q[:, 1:]), K_c_age, q[:, 1:]]))
# diagonalise centered penalty matrix
K_d_c_age, U_c_age = pen.reg_matrix_diag(S_c_age)
# calculate matrix for data manipulation (and parameters/weights)
ZU_c_age = np.float32(np.matmul(q[:, 1:], U_c_age))
data_c_age = np.float32(np.matmul(design_matrix_c_age, ZU_c_age))
# data_c_age_gcv = np.float32(np.matmul(design_matrix_c_age, U_c_age))

base_c_breastf = bs.build_base(c_breastf_d, n_knots=n_knots, degree=degree_bsplines)
design_matrix_c_breastf = base_c_breastf[0]
# Constrain matrix C
C_c_breastf = design_matrix_c_breastf.sum(axis=0).reshape(-1, 1)
# QR of C -> leave out first column of q
q, r = np.linalg.qr(C_c_breastf, mode="complete")
# penalty matrix without centering -> dim p
K_c_breastf = pen.reg_matrix(penalty_diff_order, dim=dim_reg_matrix)
# penalty matrix with centering -> dim (p-1)
S_c_breastf = np.float32(
    np.linalg.multi_dot([np.transpose(q[:, 1:]), K_c_breastf, q[:, 1:]])
)
# diagonalise centered penalty matrix
K_d_c_breastf, U_c_breastf = pen.reg_matrix_diag(S_c_breastf)
# calculate matrix for data manipulation (and parameters/weights)
ZU_c_breastf = np.float32(np.matmul(q[:, 1:], U_c_breastf))
data_c_breastf = np.float32(np.matmul(design_matrix_c_breastf, ZU_c_breastf))
# data_c_breastf_gcv = np.float32(np.matmul(design_matrix_c_breastf, U_c_breastf))

## until here already takes long to compute!!!

## first two columns of U-matrices are differently computed on server (see test.py) even if np-version is the same!
# np.savetxt("U_c_age.txt", U_c_age)
# np.savetxt("K_d_c_age.txt", K_d_c_age)
# np.savetxt("K_c_age.txt", K_c_age)
# np.savetxt("design_matrix_c_age.txt", design_matrix_c_age)
# np.savetxt("ZU_c_age.txt", ZU_c_age)

# np.savetxt("U_c_breastf.txt", U_c_breastf)
# np.savetxt("K_d_c_breastf.txt", K_d_c_breastf)
# np.savetxt("K_c_breastf.txt", K_c_breastf)
# np.savetxt("design_matrix_c_breastf.txt", design_matrix_c_breastf)
# np.savetxt("ZU_c_breastf.txt", ZU_c_breastf)

######################################
# finding start values for reg_param #
######################################


startval_c_age = {}
for i in range(len(grid)):
    startval_c_age[grid[i]] = gcv.gcv_1d(
        y=labels, design_matrix_Z=data_c_age, reg_matrix_K=K_d_c_age, reg_param=grid[i]
    )
reg_param_c_age_init = min(startval_c_age, key=startval_c_age.get)
reg_param_c_age_init

startval_c_breastf = {}
for i in range(len(grid)):
    startval_c_breastf[grid[i]] = gcv.gcv_1d(
        y=labels,
        design_matrix_Z=data_c_breastf,
        reg_matrix_K=K_d_c_breastf,
        reg_param=grid[i],
    )
reg_param_c_breastf_init = min(startval_c_breastf, key=startval_c_breastf.get)
reg_param_c_breastf_init

# startval_c_breastf_2 = {}
# for i in range(len(grid)):
#    startval_c_breastf_2[grid[i]] = gcv.gcv_1d (y = labels,
#                                                design_matrix_Z = design_matrix_c_breastf,
#                                                reg_matrix_K = K_c_breastf,
#                                                reg_param = grid[i])


##################
# building graph #
##################


tf.compat.v1.reset_default_graph()

c_age = tf.compat.v1.placeholder(
    dtype=tf.float32, shape=[None, dim_reg_matrix_center], name="input_c_age"
)
c_breastf = tf.compat.v1.placeholder(
    dtype=tf.float32, shape=[None, dim_reg_matrix_center], name="input_c_breastf"
)
y_true = tf.compat.v1.placeholder(dtype=tf.float32, shape=[None, 1], name="y_true")

init = tf.compat.v1.truncated_normal_initializer(stddev=stddev, mean=mean, seed=None)

with tf.compat.v1.name_scope("DNN"):
    with tf.compat.v1.name_scope("spline_part"):
        with tf.compat.v1.name_scope("spline_c_age"):
            weights_c_age = tf.compat.v1.get_variable(
                name="weights_c_age",
                shape=(dim_reg_matrix_center, 1),
                initializer=init,
                dtype=tf.float32,
            )
            y_hat_c_age = tf.matmul(c_age, weights_c_age)
        with tf.compat.v1.name_scope("spline_c_breastf"):
            weights_c_breastf = tf.compat.v1.get_variable(
                name="weights_c_breastf",
                shape=(dim_reg_matrix_center, 1),
                initializer=init,
                dtype=tf.float32,
            )
            y_hat_c_breastf = tf.matmul(c_breastf, weights_c_breastf)
        with tf.compat.v1.name_scope("beta_0"):
            beta_0 = tf.compat.v1.get_variable(
                name="beta_0", initializer=((beta_0_init)), dtype=tf.float32
            )
        y_hat_splines = beta_0 + y_hat_c_age + y_hat_c_breastf
    y_hat = y_hat_splines


with tf.compat.v1.name_scope("loss"):
    sq_diff = tf.reduce_sum(input_tensor=(y_true - y_hat) ** 2)
    reg_param_c_age = tf.Variable(
        reg_param_c_age_init, name="lambda_c_age", dtype=tf.float32, trainable=False
    )
    regularizer_c_age = pen.pen(
        weight_matrix=weights_c_age, reg_matrix=K_d_c_age, reg_param=reg_param_c_age
    )
    reg_param_c_breastf = tf.Variable(
        reg_param_c_breastf_init,
        name="lambda_c_breastf",
        dtype=tf.float32,
        trainable=False,
    )
    regularizer_c_breastf = pen.pen(
        weight_matrix=weights_c_breastf,
        reg_matrix=K_d_c_breastf,
        reg_param=reg_param_c_breastf,
    )
    #    centering_c_age = tf.reduce_sum(tf.matmul(data_c_age, weights_c_age))**2
    #    centering_c_breastf = tf.reduce_sum(tf.matmul(data_c_breastf, weights_c_breastf))**2
    #    loss = sq_diff + regularizer_c_age + regularizer_c_breastf + centering_c_age + centering_c_breastf
    loss = sq_diff + regularizer_c_age + regularizer_c_breastf


with tf.compat.v1.name_scope("train"):
    optimizer = tf.compat.v1.train.AdamOptimizer(
        learning_rate=learning_rate_adam, beta1=0.9, beta2=0.999, epsilon=1e-05
    )
    training_op = optimizer.minimize(loss)

with tf.compat.v1.name_scope("init"):
    global_init = tf.compat.v1.global_variables_initializer()

# max_to_keep defaults to 5
saver = tf.compat.v1.train.Saver(
    [beta_0, weights_c_age, weights_c_breastf, reg_param_c_age, reg_param_c_breastf],
    max_to_keep=100,
)

###################
# executing graph #
###################

with tf.compat.v1.Session() as sess:
    # writer = tf.summary.FileWriter("logs_b-splines/", sess.graph)
    global_init.run()

    reg_param_c_age_est = reg_param_c_age_init
    reg_param_c_breastf_est = reg_param_c_breastf_init

    for i in range(num_steps_gcv):
        deriv = gcv.d_gcv_2d(
            y=labels,
            design_matrix_Z_1=data_c_age,
            reg_matrix_K_1=K_d_c_age,
            reg_param_1=reg_param_c_age_est,
            design_matrix_Z_2=data_c_breastf,
            reg_matrix_K_2=K_d_c_breastf,
            reg_param_2=reg_param_c_breastf_est,
        )

        reg_param_c_age_est = reg_param_c_age_est - learning_rate_gcv * deriv[0]
        reg_param_c_age = tf.compat.v1.assign(reg_param_c_age, reg_param_c_age_est)

        reg_param_c_breastf_est = reg_param_c_breastf_est - learning_rate_gcv * deriv[1]
        reg_param_c_breastf = tf.compat.v1.assign(
            reg_param_c_breastf, reg_param_c_breastf_est
        )

        for j in range(num_epochs):
            (
                _,
                weight_est_c_age,
                weight_est_c_breastf,
                beta_0_est,
                loss_estimate,
                reg_param_est_c_age,
                reg_param_est_c_breastf,
            ) = sess.run(
                fetches=[
                    training_op,
                    weights_c_age,
                    weights_c_breastf,
                    beta_0,
                    loss,
                    reg_param_c_age,
                    reg_param_c_breastf,
                ],
                feed_dict={
                    c_age: data_c_age,
                    c_breastf: data_c_breastf,
                    y_true: labels_expanded,
                },
            )
            if (j + 1) % 500 == 0:
                epoch = j + 1
                saver.save(sess, "zambia2d", global_step=epoch, write_meta_graph=False)
        if (i + 1) % 100 == 0:
            epoch = num_epochs * (i + 1)
            saver.save(sess, "zambia2d", global_step=epoch, write_meta_graph=False)
    saver.save(sess, "zambia2d", global_step=-99)
    print(
        "Weight estimates c_age:\n",
        weight_est_c_age,
        "\n",
        "Weight estimates c_breastf:\n",
        weight_est_c_breastf,
        "\n",
        "type U_c_age: ",
        type(U_c_age[0, 0]),
        "\n",
        "U_c_age:\n",
        U_c_age,
        "\n",
    )

    weight_est_c_age = np.dot(ZU_c_age, weight_est_c_age)
    weight_est_c_breastf = np.dot(ZU_c_breastf, weight_est_c_breastf)

    print(
        "Weight estimates c_age:\n",
        weight_est_c_age,
        "\n",
        "Weight estimates c_breastf:\n",
        weight_est_c_breastf,
        "\n",
        "Beta_0 estimate = ",
        beta_0_est,
        "\n",
        "Regularization parameter c_age = ",
        reg_param_est_c_age,
        "\n",
        "Regularization parameter c_breastf = ",
        reg_param_est_c_breastf,
        "\n",
        "Estimated loss = ",
        loss_estimate,
        "\n",
    )

#    writer.close()


#########################################
# restoring graph and trained variables #
#########################################

"""
with tf.Session() as sess:    
    saver = tf.train.import_meta_graph('zambia2d--99.meta')
    saver.restore(sess,tf.train.latest_checkpoint('./'))
    #for v in tf.get_default_graph().get_collection("variables"):
    #    print('From variables collection ', v)
    weight_est_c_age = sess.run('weights_c_age:0')
    weight_est_c_breastf = sess.run('weights_c_breastf:0')
    beta_0_est = sess.run('beta_0:0')
    reg_param_est_c_age = sess.run('loss/lambda_c_age:0')
    reg_param_est_c_breastf = sess.run('loss/lambda_c_breastf:0')    
    print("Weight estimates c_age:\n", weight_est_c_age, "\n",
          "Weight estimates c_breastf:\n", weight_est_c_breastf, "\n",
          "Beta_0 estimate = ", beta_0_est, "\n",
          "Regularization parameter c_age = ", reg_param_est_c_age, "\n",
          "Regularization parameter c_breastf = ", reg_param_est_c_breastf, "\n")    
    

ZU_c_age = np.loadtxt('ZU_c_age.txt', dtype = 'float32')
ZU_c_breastf = np.loadtxt('ZU_c_breastf.txt', dtype = 'float32')

weight_est_c_age = np.dot(ZU_c_age, weight_est_c_age)
weight_est_c_breastf = np.dot(ZU_c_breastf, weight_est_c_breastf)

print("Weight estimates c_age:\n", weight_est_c_age, "\n",
      "Weight estimates c_breastf:\n", weight_est_c_breastf, "\n")
"""


################
# plotting gcv #
################

"""
x_plot = np.linspace(0, 50, num = 21)
y_plot = np.linspace(0, 50, num = 21)
x_plot, y_plot = np.meshgrid(x_plot,y_plot)
x_plot.shape
y_plot.shape
xy_plot = np.array([x_plot.flatten(), y_plot.flatten()]).T

len(xy_plot)

gcv_plot = np.zeros(len(xy_plot))
for i in range(len(xy_plot)):
    gcv_plot[i] = gcv.gcv_2d(y = labels, 
                     design_matrix_Z_1 = data_c_age, 
                     reg_matrix_K_1 = K_d_c_age, 
                     reg_param_1 = xy_plot[i][0], 
                     design_matrix_Z_2 = data_c_breastf, 
                     reg_matrix_K_2 = K_d_c_breastf, 
                     reg_param_2 = xy_plot[i][1])

plot_data = pd.DataFrame({'x': x_plot.flatten(), 'y': y_plot.flatten(), 'gcv': gcv_plot})
#plot_data.to_csv('Z:\Deep_Learning\dl_code_gitlab\plot_data.csv')


gcv_plot = gcv_plot.reshape(x_plot.shape)

plot_data_2 = plot_data[plot_data.y != 0]
plot_data_2 = plot_data_2[plot_data_2.x != 0]
#plot_data_2.to_csv('Z:\Deep_Learning\dl_code_gitlab\plot_data_2.csv')
"""

"""
#filename = os.path.join(dirname, 'plot_data.csv')
filename = os.path.join(dirname, 'plot_data_2.csv')
plot_data = pd.read_csv(filename)

x_plot = np.asarray(plot_data.x)
x_plot = x_plot.reshape((20,20))
y_plot = np.asarray(plot_data.y)
y_plot = y_plot.reshape((20,20))
gcv_plot = np.asarray(plot_data.gcv)
gcv_plot = gcv_plot.reshape((20,20))

from mpl_toolkits.mplot3d import Axes3D

plt.figure()
ax = plt.gca(projection='3d')
ax.plot_surface(X = x_plot, Y = y_plot, Z = gcv_plot, cmap=plt.get_cmap("coolwarm"))  #cmap = map for colours
ax.set_ylim(50,0)
ax.set_xlabel("reg_param c_age")
ax.set_ylabel("reg_param c_breastf")
ax.set_zlabel("gcv")

plt.show()
"""

####################
# plotting splines #
####################

import matplotlib.pyplot as plt

x_plot = np.linspace(0, 60, num=600)
matrix_plot = bs.build_base(x_plot, n_knots=n_knots, degree=degree_bsplines)[0]
y_hat_c_age = np.dot(matrix_plot, weight_est_c_age)
y_hat_c_breastf = np.dot(matrix_plot, weight_est_c_breastf)
plt.plot(c_age_d, labels, "bo", x_plot, y_hat_c_age, "m", markersize=0.3)
plt.show()
plt.plot(c_breastf_d, labels, "bo", x_plot, y_hat_c_breastf, "m", markersize=0.3)
plt.show()

#######################
# Analytical Solution #
#######################


gamma_hat_p_c_breastf = np.dot(
    np.matmul(
        np.linalg.inv(
            np.matmul(np.transpose(design_matrix_c_breastf), design_matrix_c_breastf)
            + reg_param_est_c_breastf * K_c_breastf
        ),
        np.transpose(design_matrix_c_breastf),
    ),
    labels,
)


gamma_hat_p_c_age = np.dot(
    np.matmul(
        np.linalg.inv(
            np.matmul(np.transpose(design_matrix_c_age), design_matrix_c_age)
            + reg_param_est_c_age * K_c_age
        ),
        np.transpose(design_matrix_c_age),
    ),
    labels,
)


####################
# plotting splines #
####################

x_plot = np.linspace(0, 60, num=600)
matrix_plot = bs.build_base(x_plot, n_knots=n_knots, degree=degree_bsplines)[0]

y_hat_c_age = np.dot(matrix_plot, weight_est_c_age)
y_hat_c_breastf = np.dot(matrix_plot, weight_est_c_breastf)
y_hat_analyt_c_breastf = np.dot(matrix_plot, gamma_hat_p_c_breastf)
y_hat_analyt_c_age = np.dot(matrix_plot, gamma_hat_p_c_age)

# plt.plot(c_age_d, labels, 'bo', x_plot, y_hat_c_age, 'm', markersize=0.3)
plt.plot(
    c_age_d,
    labels,
    "bo",
    x_plot,
    y_hat_analyt_c_age,
    "c--",
    x_plot,
    y_hat_c_age,
    "m",
    markersize=0.3,
)
plt.savefig(fname="zambia_age_" + str(50000) + ".png")
plt.show()
# plt.plot(c_breastf_d, labels, 'bo', x_plot, y_hat_c_breastf, 'm', markersize=0.3)
plt.plot(
    c_breastf_d,
    labels,
    "bo",
    x_plot,
    y_hat_analyt_c_breastf,
    "c--",
    x_plot,
    y_hat_c_breastf,
    "m",
    markersize=0.3,
)
plt.savefig(fname="zambia_breastf_" + str(50000) + ".png")
plt.show()
