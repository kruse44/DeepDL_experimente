# -*- coding: utf-8 -*-
"""
Created on Mon Aug 12 10:30:06 2019

@author: L. Dammann
"""
import numpy as np

def single_b_spline (x, knots, degree, index):
    res = np.repeat(0, len(x))
## building indicator function
    if(degree < 0.5):
        for i in range(len(x)):
            res[i] = 1  * (x[i] >= knots[index] and x[i] < knots[index+1])      # TRUE = 1, FALSE = 0  
## Recursive call to single.b.spline from inside the same function:             # index is j-l in formula from book
    else:
        res = (x-knots[index]) / (knots[index+degree]-knots[index]) * single_b_spline(x, knots, degree-1, index)
        res = res + (knots[index+degree+1]-x)/(knots[index+degree+1]-knots[index+1])*single_b_spline(x, knots, degree-1, index+1)
    return(res)


def build_base (x, n_knots, degree, knot_type="equi", given_knots=None):
  '''
  x: vector of the values of the predictor variable
  Returns a list where first element (base) is the design matrix and the
  second element (inner_knots) is a vector of the inner knots.    
  '''
## building the knots:
  ## building inner knots:
  if (knot_type == "equi"):
    inner_knots = np.linspace(start = min(x), stop = max(x), num = n_knots)     # min(x) & max(x) sind knots
  if (knot_type == "given"):
    inner_knots = given_knots
  
  ## building outer knots - 2*degree outer knots needed to construct B spline basis:
  min_dist = min(np.diff(inner_knots))
  left_outer_knots = np.linspace(start = min(x)-degree*min_dist, stop = min(x)-min_dist, num = degree)
  right_outer_knots = np.linspace(start = max(x)+min_dist, stop = max(x)+degree*min_dist, num = degree)
  
  ## building vector of all knots
  knots = np.append(arr=left_outer_knots, values = inner_knots)
  knots = np.append(arr=knots, values = right_outer_knots)
  #knots = np.append(arr = left_outer_knots, values = [inner_knots, right_outer_knots]) # fehlerhaft!!
  
## Which values for the index argument do we have to plug in the single.b.spline function?
  # setting index vector
  # designmatrix has d = n_knots + degree - 1 columns, lenth(index)=d
  index = np.arange(start = 0, stop = n_knots + degree - 1, step = 1)
  
## Building the base:
  base = single_b_spline(x, knots, degree, index[0])
  for i in index[1:]: 
    base = np.c_[base, single_b_spline(x, knots, degree, i)]
  return list([base, inner_knots])

