# -*- coding: utf-8 -*-
"""
GCV

Created on Wed Oct  9 09:37:32 2019

@author: L. Dammann
"""
import numpy as np


def d_gcv (y, design_matrix_Z, reg_matrix_K, reg_param):
    n = y.shape[0]
    G = design_matrix_Z.T @ design_matrix_Z + reg_param * reg_matrix_K
    G_inv = np.linalg.inv(G)   
    S = design_matrix_Z @ G_inv @ design_matrix_Z.T
    
    alpha = np.dot(y - np.dot(S, y), y - np.dot(S, y))       
    d_alpha = np.dot(2*(y - np.dot(S, y)), 
                     np.dot(design_matrix_Z @ G_inv @ reg_matrix_K @ G_inv @ design_matrix_Z.T, y))
    
    delta = (n - np.trace(S)) ## ohne Quadrat!!
    d_delta = np.trace(design_matrix_Z @ G_inv @ reg_matrix_K @ G_inv @ design_matrix_Z.T)
    
    d_gcv = n/delta**2 * (d_alpha - 2*alpha/delta * d_delta)    
    gcv = n * alpha / delta**2
    return (d_gcv, gcv)    


# np.amax(matrix) -- return max value of an array
    

def d_gcv_2d (y, design_matrix_Z_1, reg_matrix_K_1, reg_param_1, design_matrix_Z_2, reg_matrix_K_2, reg_param_2):
   
    design_matrix_Z = np.block([design_matrix_Z_1, design_matrix_Z_2])
    G1 = design_matrix_Z_1.T @ design_matrix_Z_1 + reg_param_1 * reg_matrix_K_1
    G2 = design_matrix_Z_1.T @ design_matrix_Z_2
    G3 = design_matrix_Z_2.T @ design_matrix_Z_1
    G4 = design_matrix_Z_2.T @ design_matrix_Z_2 + reg_param_2 * reg_matrix_K_2
    G = np.block([[G1, G2],[G3, G4]])
    G_inv = np.linalg.inv(G)
    S = design_matrix_Z @ G_inv @ design_matrix_Z.T
    
    n = y.shape[0]
    alpha = np.dot(y - np.dot(S, y), y - np.dot(S, y))
    delta = (n - np.trace(S))
    
    # d_1: partial derivative with respect to reg_param_1, d_2 equivalent
    q = reg_matrix_K_1.shape[0]
    r = reg_matrix_K_2.shape[0]
    d_1_S = -design_matrix_Z @ G_inv @ np.block([[reg_matrix_K_1, np.zeros((q, r))], [np.zeros((r, q+r))]]) @ G_inv @ design_matrix_Z.T
    d_2_S = -design_matrix_Z @ G_inv @ np.block([[np.zeros((q, q+r))], [np.zeros((r, q)), reg_matrix_K_2]]) @ G_inv @ design_matrix_Z.T
    
    d_1_alpha = np.dot(2*(y - np.dot(S, y)), np.dot(d_1_S, -y))
    d_2_alpha = np.dot(2*(y - np.dot(S, y)), np.dot(d_2_S, -y))
    
    d_1_delta = -np.trace(d_1_S)
    d_2_delta = -np.trace(d_2_S)
       
    d_1_gcv = n/delta**2 * (d_1_alpha - 2*alpha/delta * d_1_delta)
    d_2_gcv = n/delta**2 * (d_2_alpha - 2*alpha/delta * d_2_delta)
    gcv = n * alpha / delta**2
    return (d_1_gcv, d_2_gcv, gcv)

def gcv_2d (y, design_matrix_Z_1, reg_matrix_K_1, reg_param_1, design_matrix_Z_2, reg_matrix_K_2, reg_param_2):
   
    design_matrix_Z = np.block([design_matrix_Z_1, design_matrix_Z_2])
    G1 = design_matrix_Z_1.T @ design_matrix_Z_1 + reg_param_1 * reg_matrix_K_1
    G2 = design_matrix_Z_1.T @ design_matrix_Z_2
    G3 = design_matrix_Z_2.T @ design_matrix_Z_1
    G4 = design_matrix_Z_2.T @ design_matrix_Z_2 + reg_param_2 * reg_matrix_K_2
    G = np.block([[G1, G2],[G3, G4]])
    G_inv = np.linalg.inv(G)
    S = design_matrix_Z @ G_inv @ design_matrix_Z.T
    
    n = y.shape[0]
    alpha = np.dot(y - np.dot(S, y), y - np.dot(S, y))
    delta = (n - np.trace(S))
    
    gcv = n * alpha / delta**2
    return (gcv)


def gcv_1d (y, design_matrix_Z, reg_matrix_K, reg_param):
    
    G = design_matrix_Z.T @ design_matrix_Z + reg_param * reg_matrix_K
    G_inv = np.linalg.inv(G)   
    S = design_matrix_Z @ G_inv @ design_matrix_Z.T
    
    n = y.shape[0]
    alpha = np.dot(y - np.dot(S, y), y - np.dot(S, y))        
    delta = (n - np.trace(S))
       
    gcv = n * alpha / delta**2
    return (gcv)

